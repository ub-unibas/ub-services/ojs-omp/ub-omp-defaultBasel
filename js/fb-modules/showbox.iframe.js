/**
 * @author trixta
 */
(function($) {

	$.fn.showbox.defaults.iframe = {
		dims: {
			width: 600,
			height: 400
		},
		attrs: {
			frameborder: 'no'
		},
		viewHeightCorrect: 0,
		viewWidthCorrect: 0
	};

	$.createUrlIndex.mmContent.add('iframe', {
		filter: function(url, opener, urlPart) {
			return opener.is('.iframe, .iframe-box');
		},
		load: function(url, opener, ui, fn) {
			var inst = ui.instance || ui,
				jElm = $([]),
				opts = inst.options.iframe;

			var elmOpts = opener.data('showbox') || {},
				dims = $.extend({}, opts.dims),
				attrs = $.extend({}, opts.attrs, elmOpts.attrs || {});

			if (elmOpts.height) {
				dims.height = parseInt(elmOpts.height, 10);
			}
			if (elmOpts.width) {
				dims.width = parseInt(elmOpts.width, 10);
			}

			if (dims.width === 'view') {
				dims.width = $(window).width() - opts.viewWidthCorrect;
			}
			if (dims.height === 'view') {
				dims.height = $(window).height() - opts.viewHeightCorrect;
			}


			inst.content = inst.content || {};

			if (ui.extras) {
				ui.extras.mm = jElm;
			}


			inst.content = {
				'multimedia-box': function(name, element, content, isClone) {
					var elem = $('<div></div>').css(dims);

					$('div.multimedia-box', element).html(elem);
					if (!isClone) {
						$('<iframe src="' + url + '"></iframe>')
							.attr(attrs)
							.css(dims)
							.appendTo(elem);
						if (ui.extras) {
							ui.extras.mm = elem;
						}

					}
				}
			};
			inst.content['multimedia-box'][0] = dims;
			$.extend(inst.content['multimedia-box'], dims);


			fn(url, dims.width);
		}
	});

})(window.webshims && webshims.$ || jQuery);
