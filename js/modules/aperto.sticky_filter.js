/* Author: Jose Burgos */
(function($) {
	"use strict";
	/*-
	 * 
	-*/
	$.widget('aperto.stickyFilter', {
		options: {
			menuWrapper: "#global-search-filter",
			disableClass: "filter-closed"
		},
		_create: function() {
			this.offsetFilter = Math.round(this.element.offset().top);
			this.searchFilter = $(this.options.menuWrapper);
			this._bindEvents();
			this._make();
		},
		_bindEvents: function() {
			var that = this;
			var o = this.options;
			$(window)
				.on('scroll resize', $.Aperto.throttle(function(e) {
					that._make();
				}, 50));

			this.searchFilter.on("activateWidget", function(event) {

				that.element.removeClass(o.disableClass);
			})

			this.searchFilter.on("deactivateWidget", function(event) {

				that.element.addClass(o.disableClass);
			})
		},
		_make: function() {
			this.sticky = Math.round($(window).scrollTop()) >= this.offsetFilter;
			this.mobile = Modernizr.mq('(max-width: 767px)');

			if (this.sticky && this.mobile) {
				this.element.addClass('sticky');
			} else {
				this.element.removeClass('sticky');
			}
		}
	});
})(window.webshims && webshims.$ || jQuery);
