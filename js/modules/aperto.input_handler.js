/* Author: Jose Burgos */
(function($) {
	"use strict";
	/*-
	 * 
	-*/
	$.widget('aperto.inputHandler', {
		options: {
			isDebug: false,
			height: 200,
			width: "100%",
			activeClass: "active",
			masterSelectClass: "col-master-select",
			subSelectClass: "col-sub-select",
			comboboxSelector: "select.custom",
			enableAria: false,
			combobox: {
				drawDownBtn: false
					/*,
									assignSelectWidth: false*/
			}
		},
		_create: function() {
			var that = this,
				o = this.options;
			this.$el = $(this.element[0]);
			this.$selectEl = $(o.comboboxSelector, this.$el);

			this.$selectEl.combobox(o.combobox);

			this._bindEvents();
		},
		_bindEvents: function() {
			var callbackFunction = this.$el.hasClass(this.options.masterSelectClass) ? this._onChangeMainCombobox.bind(this) : this._filterActions.bind(this);

			this.$el.on("inputHandler:enable", this._enable.bind(this));
			this.$selectEl.on("change", callbackFunction);
			if (this.$el.hasClass(this.options.subSelectClass)) {
				$(window.document).on("inputHandler:subSelect:disable", this._disable.bind(this));
			}
		},
		_onChangeMainCombobox: function(event) {
			this.isDebug && console.log("Main selectbox change. Val: ", event);
			var targetId = this._getSelectedId();
			$(window.document).trigger("inputHandler:subSelect:disable");
			if (typeof targetId !== "undefined")
				$("#" + targetId).trigger("inputHandler:enable");
			this._filterActions(event);
		},
		_filterActions: function(event) {
			this.isDebug && console.log("Standard selectbox change. Val: ", val);
			// if this select input is meant to cnange another input
			// do not submit form
			if (this.$el.is(".option-link")) return;

			event.target.form.submit();
		},
		_getSelectedId: function() {
			return this.$selectEl.find(":selected").attr("rel");
		},
		_enable: function() {
			this.isDebug && console.log("enable", this);
			this.$selectEl.prop('disabled', false);
			this.$el.addClass(this.options.activeClass);
		},
		_disable: function(event, data) {
			this.isDebug && console.log("disable", this);
			this.$selectEl.prop('disabled', true);
			this.$el.removeClass(this.options.activeClass);
		}
	});
})(window.webshims && webshims.$ || jQuery);
