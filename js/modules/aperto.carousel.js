/*CUSTOM FOR UNIBAS*/
(function($) {
	"use strict";
	var PAONCLASS = 'on'; // pagination on class
	$.widget('aperto.carousel', {
		options: {
			selectedIndex: 0,
			type: 'normal',
			hasThumbView: false,
			isThumbView: false,
			pagination: true,
			paginationGrouping: false,
			itemAdjust: true,
			viewportBreaks: {
				800: 1,
				1270: 1,
				1600: 1,
				1900: 1
			},
			paginationAtoms: '<li class="pagination-item pa-$number" data-pa-index="$index"><button class="pagination-item-button"><span>$number</span></button></li>',
			autoplay: false,
			nextBtn: '.stage-next',
			prevBtn: '.stage-prev',
			paginationSel: '.pagination',
			view: '.carousel-wrapper',
			doc: '.carousel-area',
			itemSel: '.carousel-item > .carousel-item-inner'
		},
		_create: function() {
			var that = this;
			this.$view = this.element.find(this.options.view);
			this.$doc = this.$view.find(this.options.doc);
			this.viewBox = {};
			this.isAnimated = this.$doc.is('.animate');
			this.hasThumbView = this.options.hasThumbView;
			this.isThumbView = this.options.isThumbView;

			this.updateViewBox();
			this.addResize();
			this.itemAdjust(true);
			this.addTouchControls();

			if (!this.isThumbView) {
				this.pagination(true);
			} else {
				this.thumbnailPagination(true);
			}
			this.addStepButtons();
			this._created = true;

			//ENDLESS
			if (this.options.type === 'endless') {
				this.updateDisabledState();
			}

			// HAS THUMBVIEW
			if (this.hasThumbView) {
				var thumbViewID = this.element.attr('rel');
				this.thumbView = $('#' + thumbViewID);

				this.thumbView.find(this.options.itemSel).on('click', function(e) {
					e.preventDefault();
					var newSelIndex = $(this).parent().index();
					that.selectedIndex(newSelIndex);
					that.updatePagination();
					$(window).trigger('thumbnailclicked', newSelIndex);
				});
			}

			//IS THUMBVIEW
			if (this.isThumbView) {
				$(that.$doc.find(that.options.itemSel)[0]).addClass(PAONCLASS);
				$(window).on('thumbnailclicked', function(e, idx) {
					var items = that.$doc.find(that.options.itemSel);
					items.removeClass(PAONCLASS);
					$(items[idx]).addClass(PAONCLASS);
				});
			}

			//PREV-NEXT BTNS
			if (!this.isThumbView) {
				$(this.options.prevBtn, this.element).addClass('enabled').attr('tabindex', '-1');
				$(this.options.nextBtn, this.element).addClass('enabled').attr('tabindex', '-1');
			} else {
				$(this.options.prevBtn, this.element).remove();
				$(this.options.nextBtn, this.element).remove();
			}

			//AUTOPLAY
			if (this.options.autoplay) {
				this.initAutoplay();
			}
		},
		pagination: function(value) {
			if (!value) return;
			var o = this.options;

			var that = this;

			this.pagination = $(this.options.paginationSel, this.element);

			// create
			var content = '<ul class="pagination-item-list">';


			this.$doc.children().each(function(i) {
				content += o.paginationAtoms.replace(/\$number/g, i + 1).replace(/\$index/g, i);
			});


			this.pagination.html(content + '</ul>');

			this.paginationBtns = $('li', this.pagination);
			this.paginationBtns.eq(this.selectedIndex()).addClass(PAONCLASS);


			setTimeout(function() {
				that.updatePagination();
			});

			if (o.paginationGrouping) {
				$(window).on('resize', $.Aperto.throttle(function(e) {
					that.updatePagination();
				}));
			}

			this.paginationBtns.touchClick(function() {
				var $elem = $(this);
				var selectedIndex = that.selectedIndex();
				var newSelectedIndex = that.paginationBtns.index($elem);

				if (newSelectedIndex === selectedIndex) return;

				that.selectedIndex(newSelectedIndex);
				that.updatePagination();
				if (that.hasThumbView) {
					that.updateThumbview(newSelectedIndex);
				}
			});

			if (this.$doc.children().length <= 1) {
				this.pagination.addClass('disabled');
			}
		},
		updateThumbview: function(newSelectedIndex) {

			var itemLen = this.$doc.find('.carousel-item').length;
			var slidesLen = this.thumbView.find('.pagination-item').length;

			this.thumbView.find('.carousel-item-inner').removeClass(PAONCLASS);
			$(this.thumbView.find('.carousel-item-inner')[newSelectedIndex]).addClass(PAONCLASS);
			var activeSlide = Math.ceil((newSelectedIndex + 1) / (itemLen / slidesLen));
			$(this.thumbView.find('.pagination-item')[activeSlide - 1]).trigger('click');
		},
		thumbnailPagination: function(value) {
			if (!value) return;
			var o = this.options;

			var that = this;

			this.pagination = $(this.options.paginationSel, this.element);

			// create
			var content = '<ul class="pagination-item-list">';

			that.slidesCount = ((this.$doc.children().length) / this.options.viewportBreaks['800']);
			for (var i = 0; i < that.slidesCount; i++) {
				content += o.paginationAtoms.replace(/\$number/g, i + 1).replace(/\$index/g, i);
			}


			this.pagination.html(content + '</ul>');

			this.paginationBtns = $('li', this.pagination);

			that.paginationBtns.removeClass(PAONCLASS);
			$(that.paginationBtns[0]).addClass(PAONCLASS);

			this.paginationBtns.touchClick(function() {
				var $elem = $(this);
				var newSelectedIndex = ($elem.index() * (4));
				that.paginationBtns.removeClass(PAONCLASS);
				$elem.addClass(PAONCLASS);
				that.selectedIndex(newSelectedIndex);
			});
		},
		updatePagination: function() {
			var that = this;
			var selected = this.options.paginationGrouping ?
				that.getSelectedIndexes() : [that.selectedIndex(), that.selectedIndex()];

			that.paginationBtns.removeClass(PAONCLASS);
			that.paginationBtns.slice(selected[0], selected[1] + 1).addClass(PAONCLASS);
		},
		indexFromLeft: function(correct) {
			if (!correct) {
				correct = 0;
			}
			var index = 0;
			var left = (this.position() + correct) * -1;

			this.$doc.children().each(function(i) {
				var pos = $(this).position();
				if (!pos) {
					this.error('something happened');
					index = i;
					return false;
				}
				if (pos.left + 1 >= left) {

					index = i;
					return false;
				}
			});

			return index;
		},
		indexFromRight: function(correct) {
			if (!correct) {
				correct = 0;
			}
			var pos;
			var left = (this.position() + correct) * -1;
			var childs = this.$doc.children();
			var index = childs.length;

			while (index--) {
				if (!index) {
					break;
				}
				pos = $(childs[index]).position();
				if (!pos) {
					this.error('something happened');
					break;
				}
				if (pos.left - 1 <= left) {
					break;
				}
			}
			return index;
		},
		snapTo: function(rel) {

			var index;
			if (rel < 0) {
				index = this.indexFromLeft();
			} else {
				index = this.indexFromRight();
			}

			this.selectedIndex(index);

			this.updatePagination();
		},
		setRelPos: function(pos) {
			this.setAnimate(false);

			this.position(this.position() + pos);
		},
		setAnimate: function(animate) {
			if (animate != this.isAnimated) {
				this.isAnimated = animate;
				this.$doc[animate ? 'addClass' : 'removeClass']('animate');
			}
		},
		updateViewBox: function() {
			this.adjustItems();
			this.viewBox.min = 0;
			this.viewBox.width = this.$view.width();
			var max = this.$doc.outerWidth(true) - this.viewBox.width;


			if (max !== this.viewBox.max) {
				this.viewBox.max = max;
				this.selectedIndex(this.options.selectedIndex, true);
			}
		},
		selectNext: function() {
			var currIndex = this.selectedIndex();
			var len = this.$doc.children().length;
			var newIndex = currIndex + 1;
			if (currIndex >= len - 1) {
				newIndex = 0;
			}
			this.selectedIndex(newIndex);
		},
		selectPrev: function() {
			var currIndex = this.selectedIndex();
			var len = this.$doc.children().length;
			var newIndex = currIndex - 1;
			if (currIndex <= 0) {
				newIndex = len - 1;
			}
			this.selectedIndex(newIndex);
		},
		getSelectedIndexes: function() {
			var $childs, curEnd, i;
			if (!this._selectedIndexes) {
				curEnd = Math.abs(this.position());
				$childs = this.$doc.children();
				this._selectedIndexes = [this.options.selectedIndex, this.options.selectedIndex];
				if (!this.reachedEnd()) {
					curEnd += this.viewBox.width;
					for (i = this.options.selectedIndex; i < $childs.length; i++) {
						if ($childs.get(i).offsetLeft + i + 5 >= curEnd) {
							break;
						}
						this._selectedIndexes[1] = i;
					}
				} else {
					this._selectedIndexes[1] = [$childs.length - 1];
					for (i = $childs.length - 1; i >= 0; i--) {
						this._selectedIndexes[0] = i;

						if ($childs.get(i).offsetLeft - i - 5 <= curEnd) {
							break;
						}
					}
				}
			}

			return this._selectedIndexes;
		},
		selectedIndex: function(index, noAimate) {
			var left, childs;
			if (arguments.length) {
				childs = this.$doc.children();
				if (index < 0 || childs.length <= index || (!this._created && !index && this.options.selectedIndex == index)) {
					return;
				}
				left = childs.eq(index).position();
				if (!left) {
					this.error('something bad happened');
					return;
				}
				left = Math.min(Math.max(left.left, this.viewBox.min), this.viewBox.max) * -1;

				this.setAnimate(this._created && !noAimate);

				this.position(left);

				this._selectedIndexes = null;

				if (this.options.type == 'normal') {
					this.updateStartEnd();
				}

				if (this.options.selectedIndex != index) {
					this.options.selectedIndex = index;
					this.element.trigger('indexchange');
				}
			} else {
				return this.options.selectedIndex;
			}
		},
		position: function(pos) {
			if (arguments.length) {
				this.$doc[0].style.left = pos + 'px';
			} else {
				return parseFloat(this.$doc.css('left'), 10) || 0;
			}
		},
		updateStartEnd: function() {
			var start = this.position() * -1 < 1;
			var end = this.position() * -1 > this.viewBox.max - 1;
			if (start !== this._atStart || end !== this._atEnd) {
				this._atStart = start;
				this._atEnd = end;
				if (this.options.type === 'endless') {
					this.updateDisabledState();
				}
			}
		},
		reachedStart: function() {
			return this._atStart;
		},
		reachedEnd: function() {
			return this._atEnd;
		},
		addStepButtons: function() {
			var carousel = this;

			this.element.touchClick(this.options.nextBtn, function() {
				carousel.selectNext();
				carousel.updatePagination();
				if (carousel.hasThumbView) {
					carousel.updateThumbview(carousel.selectedIndex());
				}
			});
			this.element.touchClick(this.options.prevBtn, function() {
				carousel.selectPrev();
				carousel.updatePagination();
				if (carousel.hasThumbView) {
					carousel.updateThumbview(carousel.selectedIndex());
				}
			});

			if (this.options.type === 'endless') {
				this.updateDisabledState();
			}
		},
		updateDisabledState: function() {
			$(this.options.prevBtn, this.element).prop('disabled', this.reachedStart());
			$(this.options.nextBtn, this.element).prop('disabled', this.reachedEnd());
		},
		addResize: function() {
			var that = this;
			$(window).on('resize', $.Aperto.throttle(function(e) {
				that.updateViewBox();
			}));
		},
		addTouchControls: function() {
			var that = this;
			var startX, lastX, lastY, clickTimer;
			var carousel = this;
			var $doc = this.$doc;
			var stopClick = false;
			var allowClick = function() {
				stopClick = false;
			};
			var preventClick = function() {
				stopClick = true;
				clearTimeout(clickTimer);
				clickTimer = setTimeout(allowClick, 301);
			};
			var end = function(e) {
				var evt = e.originalEvent;
				var touches = (evt.changedTouches || evt.touches);

				if (touches.length == 1) {
					if (Math.abs(lastX - startX) > 15) {
						preventClick();
						e.preventDefault();
						carousel.snapTo(lastX - startX);
						destroy();
					}
				}
			};
			var move = function(e) {
				var evt = e;
				e = e.originalEvent;
				var touch = (e.changedTouches || e.touches)[0];
				var x = touch.pageX;
				var y = touch.pageY;
				var difX = lastX - x;
				var difY = lastY - y;
				lastX = x;
				lastY = y;

				if (Math.abs(difX)) {
					if (!Math.abs(difY)) {
						evt.preventDefault();
					}
					carousel.setRelPos(difX * -1);
				}
			};
			var destroy = function() {
				$doc.off('touchmove', move).off('touchend', end);
			};

			$doc
				.on('click', function(e) {
					if (stopClick || that.isThumbView) {
						e.preventDefault();
						e.stopImmediatePropagation();
					}
				})
				.on('touchstart', function(e) {
					e = e.originalEvent;

					if (e.touches.length == 1) {
						destroy();
						startX = e.touches[0].pageX;
						lastX = e.touches[0].pageX;
						lastY = e.touches[0].pageY;
						$doc.on('touchmove', move);
						$doc.on('touchend', end);
					}
				});
		},
		itemAdjust: function(value) {
			if (arguments.length && value !== false) {
				this.adjustItems();
				this.updateViewBox();
			}
		},
		adjustItems: function() {
			var itemWidth;
			var itemLength = 1;
			var viewport = this.$view.innerWidth();
			var $items = $(this.options.itemSel, this.$view);

			$.each(this.options.viewportBreaks, function(itemBreak, len) {
				itemLength = len;
				if (viewport <= itemBreak) {
					return false;
				}
			});
			if (itemLength) {
				itemWidth = (viewport / itemLength) - (parseFloat($($items[1]).css('marginLeft'), 10) || 0) - (parseFloat($($items[1]).css('marginRight'), 10) || 0);
				$items.outerWidth(itemWidth);
			} else {
				$items.css({
					width: ''
				});
			}
		},
		initAutoplay: function() {
			var that = this;
			that.playPauseBtn = $('<span tabindex="0" role="button" class="play-pause-btn">' + $.i18n.getText('isPlaying') + '</span>').appendTo(this.$view);
			that.playPauseBtn.addClass('isPlaying');
			var interval = 8000;

			that.playPauseBtn.on('click', function(e) {
				e.preventDefault();
				if ($(this).is('.isPlaying')) {

					$(this).removeClass('isPlaying').addClass('isPausing').text($.i18n.getText('isPausing'));

					window.clearInterval(that.myInterval);

				} else {
					$(this).removeClass('isPausing').addClass('isPlaying').text($.i18n.getText('isPlaying'));

					that.myInterval = window.setInterval(function() {
						that.selectNext();
						that.updatePagination();
					}, interval);
				}
			});

			that.myInterval = window.setInterval(function() {
				that.selectNext();
				that.updatePagination();
			}, interval);
		}
	});
})(window.webshim && webshim.$ || jQuery);
