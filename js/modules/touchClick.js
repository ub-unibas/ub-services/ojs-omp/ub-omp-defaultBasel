$.fn.touchClick = (function() {
	var supportsTouchaction = ('touchAction' in document.documentElement.style);
	var addTouch = !supportsTouchaction && ('ontouchstart' in window) && document.addEventListener;
	return function(target, handler, onlyTouch) {
		var touchData, touchEnd, touchStart, stopClick, allowClick, touchEvent;
		var runHandler = function(e) {
			if (!stopClick) {
				return handler.apply(this, arguments);
			} else if (touchEvent) {
				if (touchEvent.isDefaultPrevented()) {
					e.preventDefault();
				}
				if (touchEvent.isPropagationStopped()) {
					e.stopPropagation();
				}
				if (touchEvent.isImmediatePropagationStopped()) {
					e.stopImmediatePropagation();
				}
			}
		};

		if ($.isFunction(target)) {
			onlyTouch = handler;
			handler = target;
			target = false;
		}

		if (addTouch) {
			allowClick = function() {
				touchEvent = false;
				stopClick = false;
			};
			touchEnd = function(e) {
				var ret, touch;
				var jEvt = e;
				e = e.originalEvent || {};
				$(this).off('touchend touchcancel', touchEnd);
				var changedTouches = e.changedTouches || e.touches;
				if (e.type == 'touchcancel' || !touchData || !changedTouches || changedTouches.length != 1) {
					return;
				}

				touch = changedTouches[0];
				if (Math.abs(touchData.x - touch.pageX) > 40 || Math.abs(touchData.y - touch.pageY) > 40 || Date.now() - touchData.now > 300) {
					return;
				}

				touchEvent = jEvt;
				stopClick = true;
				setTimeout(allowClick, 400);

				ret = handler.apply(this, arguments);

				return ret;
			};

			touchStart = function(e) {
				var touch, elemTarget;
				if ((!e || e.touches.length != 1)) {
					return;
				}
				touch = e.touches[0];
				elemTarget = target ? $(touch.target).closest(target) : $(this);
				if (!elemTarget.length) {
					return;
				}
				touchData = {
					x: touch.pageX,
					y: touch.pageY,
					now: Date.now()
				};
				elemTarget.on('touchend touchcancel', touchEnd);
			};

			this.each(function() {
				this.addEventListener('touchstart', touchStart, true);
			});
		} else if (supportsTouchaction) {
			this.css('touch-action', 'manipulation');
		}

		if (!onlyTouch) {
			if (!target) {
				this.on('click', runHandler);
			} else {
				this.on('click', target, runHandler);
			}
		}
		return this;
	};
})();
