(function($) {
	"use strict";

	/**
	 * Author: Sebastian Fitzner (sebastian.fitzner@aperto.de)
	 *
	 * Set Equal Height Per Row
	 */

	$.widget("aperto.equalRowHeight", {
		options: {
			isDebug: false,
			childElements: 'li',
			lastRowClass: 'last-row',
			addPadding: false
		},
		_init: function() {
			this._bindEvents();
		},

		_create: function() {
			this.isDebug = this.options.isDebug;

			var listEl = $(this.options.childElements, this.element);

			this.isDebug && console.log('equalRowHeight');
			this._trigger('start', this);
			if (listEl.length !== 0) {
				this.buildRow(listEl);
			}
			this._trigger('complete', this);
		},

		_bindEvents: function() {
			var that = this,
				timer;

			$(document).on('equalRowHeight:init', $.Aperto.throttle(function(element) {
				//that._reinit(that, timer);
				that._create();
				that.options.isDebug && console.log('equalRowHeight:init');
			}));

			$(window).on('resize', $.Aperto.throttle(function() {
				//that._reinit(that, timer);
				that._create();
				that.options.isDebug && console.log('equalRowHeight.resize');
			}));
		},

		_reinit: function(that, timer) {
			clearTimeout(timer);
			timer = setTimeout(that._create(), 250);
		},

		_resetStyles: function(el) {
			el.removeAttr('style');
		},

		_setLastRowClass: function(element) {
			var that = this;

			$(element).each(function() {
				$(this).addClass(that.options.lastRowClass);
			});
		},

		buildRow: function(el) {
			var that = this;
			var rows = [];
			var posArray = [];
			var firstElTopPos = $(this.options.childElements).eq(0).offset().top;

			this.isDebug && console.log('equalRowHeight:buildRow');

			el.each(function() {

				var el = $(this);

				that._resetStyles(el);


				if (el.offset().top === firstElTopPos) {
					posArray.push(el);
				} else {
					rows.push(posArray);
					el.css('clear', 'left');
					posArray = [];
					posArray.push(el);
					firstElTopPos = el.offset().top;
				}

			});

			rows.push(posArray);
			this.defineRowHeight(rows);
		},

		defineRowHeight: function(rows) {
			var that = this,
				i = 0,
				padding = ~~this.options.addPadding;

			this.isDebug && console.log('equalRowHeight:defineRowHeight');

			for (i; i < rows.length; i++) {
				var height = that.getRowHeight(rows[i]);

				that.setHeight(rows[i], height, padding);

				if (i > 0 && i === rows.length - 1) {
					that._setLastRowClass(rows[i]);
				}
			}
		},

		getRowHeight: function(elements) {
			var height = 0;

			$(elements).each(function() {
				height = $(this).height() > height ? $(this).height() : height;
			});

			return height;
		},

		setHeight: function(elements, height, padding) {
			var addPadding = padding || 0;


			$(elements).each(function() {
				$(this).css({
					'height': height + addPadding
				})
			});
		}
	});

})
(window.webshims && webshims.$ || jQuery);
