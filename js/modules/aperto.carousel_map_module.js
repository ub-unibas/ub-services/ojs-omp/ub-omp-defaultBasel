/*globals Modernizr:true */
/* Author: Jose */
(function($) {
	"use strict";

	$.widget('aperto.carouselMapModule', {
		options: {
			// widget options
			template: "living",
			infowindowTemplate: "infowindow",
			infowindowMobileTemplate: "infowindow-mobile",
			slidesTemplate: "living-slides",
			tabSelector: "#tab-selector",
			toggleSelector: ".carousel-map-module",
			carouselWrapperId: "carousel-container",
			mapWrapperId: "map-module-wrapper",
			mapCanvasId: "map-module-canvas",
			loader: ".loading",
			isDebug: false,
			initialTab: 0,
			initialMode: 0, // 0 -> Carousel, 1 -> Map
			monoMode: false,
			mobileWidth: 767,
			mobileAspectRatio: 9 / 24,
			desktopAspectRatio: 420 / 1280,
			// zoom control
			zoomIn: ".zoom-control .zoom-in",
			zoomOut: ".zoom-control .zoom-out",
		},
		templates: {
			slidesList: '<div class="carousel">' +
				'<div class="carousel-wrapper">' +
				'<ul class="carousel-area"></ul>' +
				'<div class="pagination"></div>' +
				'</div>' +
				'<button class="stage-prev" type="button">prev</button>' +
				'<button class="stage-next" type="button">next</button>' +
				'</div>'
		},
		/*-
		 * initialize the handlebar templates, data arrays, initialize
		 * this widget and get the date with ajax
		 -*/
		_create: function() {
			var o = this.options;

			this.template = window.templates[o.template];
			this.slidesTemplate = Handlebars.compile(window.templates[o.slidesTemplate]);
			this.infowindowTemplate = Handlebars.compile(window.templates[o.infowindowTemplate]);
			this.infowindowMobileTemplate = Handlebars.compile(window.templates[o.infowindowMobileTemplate]);

			this.dataMakers = [];
			this.dataBounds = [];

			this.selector = o.initialTab;
			this.isDebug = o.isDebug;

			this._initialize();

			this._getData();
		},
		/*-
		 * initialize the map if:
		 ** it is the first time
		 ** there is a resize event
		 * query module elements and initialize the module
		 -*/
		_initialize: function(event) {
			var o = this.options;

			// if the current module is the carousel 
			// or the new viewport width is equal to the previous
			// if(event && this.moduleMode && (this.moduleMode === o.carouselWrapperId) || this.viewportWidth === window.innerWidth) return;
			// Do not initialize on resize event:
			if (event && event.type == "resize") return;
			if (event && this.viewportWidth === window.innerWidth) return;

			this.$el = $(this.element);
			// this.$el.html(this.template);
			this.$el[0].innerHTML = this.template();

			this.$loader = $(o.loader, this.$el);
			this._toggleLoader("on");

			this.$carouselWrapper = $("#" + o.carouselWrapperId, this.$el);
			this.$mapWrapper = $("#" + o.mapWrapperId, this.$el);
			//this.$carousel = $(o.carousel, this.$carouselWrapper);
			this.$tabSelectors = $(o.tabSelector, this.$el);
			this.$toggleSelectors = $(o.toggleSelector, this.$el);

			this.mapRendered = false;

			this._getDeviceMode(o);
			//this._calculateHeight(o);
			this._bindEvents(this, o);
			// initialize the module toggle if there is more than one mode (map || carousel)

			if (typeof(google) === 'undefined') {
				o.monoMode = true;
			}
			if (!o.monoMode && this.mode) {

				new aperto.ContainerToggle({
					selector: o.toggleSelector,
					context: this.$el
				});
			}
			if (typeof event !== "undefined")
				this._firstInit();
		},
		/*-
		 * first module initialization
		 -*/
		_firstInit: function() {
			var o = this.options;
			// select the first mode before the initialization
			this.moduleMode = o.initialMode === 0 ? o.carouselWrapperId : o.mapWrapperId;

			var $target = $(o.toggleSelector + "[data-target=" + this.moduleMode + "]", this.$el),
				event = {};

			event.target = $target;
			// if it is monoMode, the toggle selector will be deleted
			if (!o.monoMode) {
				$target.containerToggle("enableEl");
			} else {
				$target.show();
				this.$toggleSelectors.not($target).remove();
				this._initMode()
			}
		},
		/*-
		 * bind event listeners:
		 * on resize: initialize the module
		 * the module el is opened: executes initMode
		 * click on a selector 
		 -*/
		_bindEvents: function(that, o) {
			//$(window).smartresize(this._initialize.bind(this));
			$(window).on('resize', $.Aperto.throttle(function() {
				that.moduleMode = o.carouselWrapperId;
				that._selectMode(o);
				that._toggleTabSelector($('#tab-selector .carousel-map-module'));
				$('#tab-selector .carousel-map-module').trigger('click');
			}));

			this.$toggleSelectors.on("containerToggle:complete", this._initMode.bind(this));
			this.$tabSelectors.on("click", this._onClickTab.bind(this));

			$(o.zoomIn, this.$el).on("click touchstart", function(event) {
				event.preventDefault();
				var currentZoom;

				if (!that.map) {
					return;
				}

				currentZoom = that.map.getZoom();

				if (currentZoom < 30) {
					that.map.setZoom(currentZoom + 1);
				}
			});
			$(o.zoomOut, this.$el).on("click touchstart", function(event) {
				event.preventDefault();
				var currentZoom;

				if (!that.map) {
					return;
				}

				currentZoom = that.map.getZoom();

				if (currentZoom > 1) {
					that.map.setZoom(currentZoom - 1);
				}
			});
		},
		/*-
		 * get the selected mode in order to decide what to do
		 -*/
		_initMode: function(event) {
			var o = this.options,
				preselectedMode = o.initialMode === 0 ? o.carouselWrapperId : o.mapWrapperId;

			this.moduleMode = event ? $(event.target).data("target") : preselectedMode;
			this._toggleTabSelector(this.$tabSelectors.eq(this.selector));

			this._selectMode(o);
		},
		/*-
		 * decide between carousel o map mode
		 -*/
		_selectMode: function(o) {
			switch (this.moduleMode) {
				// show map
				case o.mapWrapperId:
					this.mapMode();
					break;
					// show carousel
				case o.carouselWrapperId:
				default:
					this.carouselMode();
					break;
			}
		},
		/*-
		 * init carousel mode
		 -*/
		carouselMode: function() {
			this.isDebug && console.log("initialize carousel ", this);

			this._renderCarousel(this.selector);
		},
		/*-
		 * init map mode
		 -*/
		mapMode: function() {
			this.isDebug && console.log("initialize map ", this);
			// init the map if it was not initialized
			// else it renders the markers

			//Check for GoogleApi Available
			if (typeof google === 'object' && typeof google.maps === 'object' && !this.mapRendered) {
				this._initMap();
			} else {
				this._renderCategoryMarkers();
			}
		},
		/*-
		 * on click selector tab:
		 * switch between rendering the carousel or the map
		 -*/
		_onClickTab: function(event) {
			this.isDebug && console.log("selector", event.target);
			event.preventDefault();
			var $elem = $(event.target),
				selector = $elem.data("selector");

			if ($elem.is(".active")) return;

			this.prevSelector = this.selector;
			this.selector = selector;

			this._toggleLoader("on");
			this._toggleTabSelector($elem);

			if (this.moduleMode === this.options.carouselWrapperId)
				this._renderCarousel(selector);
			else
				this._renderCategoryMarkers();
		},
		/*-
		 * Get data from server
		 -*/
		_getData: function() {
			var that = this,
				url = this.$el.data("url");

			$.ajax({
				type: "GET",
				url: url,
				dataType: "json",
				success: function(data) {
					that.isDebug && console.log("success", data);
					that.data = data.data;
					that._firstInit();
				},
				error: function(error) {
					that.isDebug && console.error("error => ", error);
				}
			});
		},
		/*-
		 * init the carousel with its handlebars template,
		 * pass the data and render the carousel
		 -*/
		_renderCarousel: function(selector) {
			var that = this,
				data = this.data[selector];

			this.$carouselWrapper.html(this.templates.slidesList);

			this.$carousel = $('.carousel', this.$carouselWrapper);
			// place names in tabs
			$.each(this.data, function(index, item) {
				that._getTabHelper(index).html(item.name);
			});

			this._renderImages(data);
			this._toggleLoader("off");

			//console.log(this.$carousel);
			this.$carousel.carousel();
		},
		/*-
		 * render carousel images
		 -*/
		_renderImages: function(data) {
			var that = this,
				slides = this.mode === "desktop" ? data.slides : data.slides;

			$.each(slides, function(index, item) {
				if (typeof item.link !== "undefined")
					item.linkTitle = $.i18n.getText('carousel').link
			});

			var $elem = this.slidesTemplate({
				slides: slides
			});
			$(".carousel-area", this.$carousel).append($elem);
		},
		/*-
		 * toggle Tab Selector
		 -*/
		_toggleTabSelector: function($elem) {
			this.$tabSelectors.removeClass("active");
			$elem.addClass("active");
			// var $targets = this.$carousel.is(":visible") ? $(":focusable", this.$carousel) : $(":focusable", this.$mapWrapper);

			// if($targets.filter("[tabindex='0']").size() > 0) {
			// 	$targets.filter("[tabindex='0']").first().focus();
			// } else {
			// 	$targets.first().focus();
			// }
		},
		/*-
		 * toggle spinner loading icon
		 -*/
		_toggleLoader: function(status) {
			if (status === "on")
				this.$loader.addClass("active");
			else
				this.$loader.removeClass("active");
		},
		/*-
		 * get tab
		 -*/
		_getTabHelper: function(selector) {
			return $(this.options.tabSelector + "[data-selector='" + selector + "']");
		},
		/*-
		 * calculate module height
		 -*/
		_calculateHeight: function(o) {
			this._getDeviceMode(o);

			var aspectRatio = this.mode === "desktop" ? o.desktopAspectRatio : o.mobileAspectRatio,
				height = this.viewportWidth * aspectRatio;

			this.isDebug && console.log("resize _calculateHeight", height);
			console.log(height, aspectRatio, this.viewportWidth);

			this.$carouselWrapper.height(height);
			this.$mapWrapper.height(height);
			this.$loader.css("margin", height / 2 + "px auto");
			this.$loader.addClass("active");
		},
		/*-
		 * initialize google maps and bind its events
		 -*/
		_initMap: function(event) {
			var that = this;
			var o = this.options;

			var pinIcon = o.devmode || !window.magnoliaFrontendData ? "img/gmaps/pin.png" : window.magnoliaFrontendData.themePath + "/img/gmaps/pin.png";
			this.pinImg = new google.maps.MarkerImage(pinIcon);

			$.extend(o, {
				googleMaps: {
					zoom: 15,
					center: new google.maps.LatLng(47.559731, 7.592213),
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					zoomControl: false,
					scrollwheel: false,
					panControl: false,
					streetViewControl: false,
					mapTypeControl: false,
					zoomControlOptions: {
						style: google.maps.ZoomControlStyle.LARGE,
						position: google.maps.ControlPosition.LEFT_BOTTOM
					}
				}
			});

			this._toggleLoader("on");

			$.webshims.ready('WINDOWLOAD', function() {
				that.isDebug && console.log("render map")
					// render map
				that.map = new google.maps.Map(document.getElementById(o.mapCanvasId), o.googleMaps);
				that._bindGMapsEvents(event, o);
			});
		},
		/*-
		 * bind google maps events
		 -*/
		_bindGMapsEvents: function(event, o) {
			var that = this;

			google.maps.event.addListenerOnce(this.map, 'tilesloaded', function() {
				// remove google links
				//$("a, span", that.map.j).remove();
				that._toggleLoader("off");
				that.mapRendered = true;
				that._renderCategoryMarkers();
			});
		},
		/*-
		 * render google maps markers for a certain category
		 -*/
		_renderCategoryMarkers: function(event) {
			this._removeInfowindow();

			var that = this,
				selector = typeof event === "undefined" ? this.selector : $(event.target).data("selector");

			this.isDebug && console.log("Render Markers", event);

			this._createGoogleMapMarkers(selector);

			this._clearMapMarkers();
			this._renderMarkers(selector);

			this._toggleLoader("off");
		},
		// Desktop functions
		_renderMarkers: function(selector) {
			this._setMapMarkers(selector);
			// fit the viewport to the current markers
			this.map.fitBounds(this.dataBounds[selector]);
			// Zoom a little bit out so that the pins will not
			// be hidden by the tab selector div
			this.map.setZoom(this.map.getZoom() - 1);
		},
		_setMapMarkers: function(selector) {
			var that = this;

			if (!this.$googleMapLink)
				this.$googleMapLink = $("[data-target=map-module-wrapper]", this.$el);
			if (this.options.monoMode)
				this.$googleMapLink.hide();

			$.each(this.dataMakers[selector], function(index, marker) {
				marker.setMap(that.map)
			});
		},
		_createGoogleMapMarkers: function(selector) {
			if (typeof this.dataMakers[selector] !== "undefined") return;

			this.dataMakers[selector] = [];

			var that = this,
				bounds = new google.maps.LatLngBounds();
			$.each(this.data[selector].markers, function(index, item) {
				var marker = new google.maps.Marker({
					position: new google.maps.LatLng(item.latitude, item.longitude),
					title: item.title,
					content: item.content,
					icon: that.pinImg,
					img: item.img,
					url: item.url
				});

				bounds.extend(marker.getPosition())
				that.dataMakers[selector].push(marker);
				that._bindGMapsInfowindowEvents(marker);
			});

			this.dataBounds[selector] = bounds;
		},
		_clearMapMarkers: function() {
			var that = this,
				markers = this.dataMakers[this.prevSelector];

			if (typeof this.prevSelector === "undefined" || typeof markers === "undefined")
				return;

			$.each(markers, function(index, marker) {
				marker.setMap(null);
			});
		},
		_setInfowindowContent: function(marker) {
			this._removeInfowindow();

			var content = {
				img: marker.img,
				title: marker.title,
				content: marker.content,
				url: marker.url
			};
			var isDesktop = this.mode === "desktop";
			var template = isDesktop ? this.infowindowTemplate : this.infowindowMobileTemplate;

			this.infowindow = new InfoBox({
				latlng: marker.getPosition(),
				map: this.map,
				content: template(content),
				width: 300,
				height: isDesktop ? 374 : 172
			});
		},
		_removeInfowindow: function() {
			// remove the gmap infowindow if exists
			if (this.infowindow) this.infowindow.remove();
		},
		_bindGMapsInfowindowEvents: function(marker) {
			var that = this;
			// google maps events
			google.maps.event.addListener(marker, 'click', function() {
				that._setInfowindowContent(marker);
				if (that.options.monoMode)
					that._setLinkLocation(marker);
			});
		},
		_setLinkLocation: function(marker) {
			var position = marker.getPosition();

			this.$googleMapLink
				.text($.i18n.getText('maps').linkText)
				.attr("target", "_blank")
				.attr("href", "https://maps.google.com/maps?daddr=" + position.d + "," + position.e + "");

			this.$googleMapLink.show();
		},
		_getDeviceMode: function(o) {
			this.viewportWidth = window.innerWidth;

			this.mode = o.mobileWidth < this.viewportWidth ? "desktop" : "mobile";
		}
	});
})(window.webshims && webshims.$ || jQuery);
