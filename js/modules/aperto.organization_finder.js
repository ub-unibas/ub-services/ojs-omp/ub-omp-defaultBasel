/* Author: Jose Burgos */
(function($) {
	"use strict";
	/*-
	 * location finder
	 [ ]
	-*/
	$.widget("aperto.organizationFinder", {
		options: {
			isDebug: false,
			handle: ".toggle",
			expandable: ".expandable_content",
			mapMainWrapper: ".main-map-wrapper",
			map: ".map",
			mapWrapper: ".map-wrapper",
			loader: ".loading",
			loaderImg: ".loader",
			pinIcon: "../img/gmaps/pin.png",
			preselected: 0
		},
		_create: function() {
			var o = this.options;
			var that = this;
			this.isDebug = o.isDebug;
			this.isDebug && console.log("initialize ", this);

			this.$el = $(this.element);
			this.$mapCanvas = $(o.map, this.$el);
			this.$mapClone = this.$mapCanvas.clone();
			this.rendered = false;



			if (typeof this.$mapCanvas === "undefined" || this.$mapCanvas.size() < 1) return;
			this._bindEvents();
		},
		_initMapElements: function() {
			if (typeof google !== 'undefined' && typeof google.maps !== 'undefinde') {


				var o = this.options;
				this.rendered = true;
				var pinIcon = o.devmode || !window.magnoliaFrontendData ? "img/gmaps/pin.png" : window.magnoliaFrontendData.themePath + "/img/gmaps/pin.png";
				this.pinImg = new google.maps.MarkerImage(pinIcon);

				this._getDeviceMode(o);
				this._calculateCanvasSize();

				$.extend(o, {
					googleMaps: {
						zoom: 15,
						center: new google.maps.LatLng(47.5667, 7.6000),
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						disableDefaultUI: true
					}
				});

				o.googleMaps.center = new google.maps.LatLng(this.$mapCanvas.data("lat"), this.$mapCanvas.data("lon"));
				// render map
				this.map = new google.maps.Map(this.$mapCanvas[0], o.googleMaps);

				this._bindGMapsEvents();

				this._setMapMarker();
			} else {
				this._showError();
			}
		},
		_showError: function() {
			$('<p class="hint-error">' + $.i18n.getText('errorMsg') + '</p>').insertAfter($(this.options.mapMainWrapper, this.element));
			$(this.options.mapMainWrapper, this.element).addClass('hint-error-wrapper');
		},
		_bindEvents: function() {
			var that = this;
			$(window).smartresize(function(event) {
				//console.log("onResizeEvent", this);
				if (that.rendered) that._resetCanvas();
			});
			// the location finder will be visible after the first time the map button is clicked

			this.$el.on("panelExpanded", function(event) {
				that._initMapElements();
			});
			this.$el.on("containerToggle:start", function(event) {
				that.isDebug && console.log("containerToggle:start", event);
				$(this).on("containerToggle:complete", function(event) {
					that.isDebug && console.log("containerToggle:complete", event);
					$.webshims.ready('WINDOWLOAD', function() {
						that._initMapElements();
					});
					$(this).off("containerToggle:complete");
				});
			});
			//that._initMapElements();
		},
		_bindGMapsEvents: function() {
			var that = this;

			google.maps.event.addListenerOnce(this.map, 'tilesloaded', function() {
				// do something only the first time the map is loaded
				$(that.options.loader, that.$el).removeClass("active");
			});
		},
		_setMapMarker: function() {
			var marker = new google.maps.Marker({
				position: this.options.googleMaps.center,
				icon: this.pinImg,
				map: this.map
			});
		},
		_calculateCanvasSize: function() {
			var o = this.options;
			var isMobile = (this.mode === "mobile") ? 1 : 0;
			var canvasPadding = isMobile ? parseInt(this.$mapCanvas.css("padding-left").replace("px", ""), 10) : 0;
			var $mapMainWrapper = $(o.mapMainWrapper, this.$el);
			var mapFloat = $mapMainWrapper.css("float");
			var canvasSize;
			var loaderSize = $(o.loaderImg, this.$el).width();
			$mapMainWrapper.css("float", "none");
			canvasSize = this.$mapCanvas.width() - canvasPadding * 2;
			$mapMainWrapper.css("float", mapFloat);

			$(this.$mapCanvas, this.$el)
				.width(canvasSize)
				.height(canvasSize);
			if (isMobile) $(this.$mapCanvas, this.$el).css("margin-left", canvasPadding);

			$(o.loader, this.$el).css("margin", (canvasSize / 2 - loaderSize / 2) + "px auto");
		},
		_resetCanvas: function() {
			var o = this.options;

			this.$mapCanvas.remove();
			this.$mapClone.attr("style", "");
			this.isDebug && console.log("Cloned Map El", this.$mapClone[0])
			$(o.mapWrapper, this.$el).html(this.$mapClone);
			this.$mapCanvas = $(this.options.map, this.$el);
			this._initMapElements();
		},
		_getDeviceMode: function(o) {
			this.mode = o.mobileWidth >= window.innerWidth ? "mobile" : "desktop";
		}
	});
})(window.webshims && webshims.$ || jQuery);
