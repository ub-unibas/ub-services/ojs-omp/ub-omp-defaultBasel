/**
 * @author alexander.farkas
 * 
 * Usage Example:
 * 
 * 

$('#photo-index a').each(function(){
	$.imgPreLoad.add($(this).attr('href'));
});

 */
(function($) {
	$.imgPreLoad = (function() {
		var srcList = [],
			ready = false,
			started = false,
			loaded = false,
			errorDelay = 5000,
			errorTimer;

		function createImg() {
			return (window.Image) ? new Image() : document.createElement('img');
		}

		function loadImg(src, callback) {
			var img = createImg(),
				fn = function(e) {
					var that = this,
						args = arguments;
					clearTimeout(errorTimer);
					$(this).unbind('load error');
					src[1].apply(that, args);
					callback.apply(that, args);
				};

			img.src = src[0];

			if (!img.complete) {
				clearTimeout(errorTimer);
				errorTimer = setTimeout(function() {
					fn.call(img, {
						type: 'timeouterror'
					});
				}, errorDelay);
				$(img)
					.bind('load error', fn);
			} else {
				fn.call(img, {
					type: 'cacheLoad'
				});
			}
		}

		function loadNextImg() {
			if (srcList.length && ready) {
				started = true;
				var src = srcList.shift();
				loadImg(src, loadNextImg);
			} else {
				started = false;
			}
		}

		function pause() {
			started = false;
			ready = false;
		}

		function restart() {
			if (loaded) {
				ready = true;
				loadNextImg();
			}
		}

		function loadNow(src, callback) {
			pause();
			callback = callback || function() {};
			loadImg([src, callback], restart);
		}

		return {
			add: function(src, fn, priority) {
				fn = fn || function() {};
				src = [src, fn];
				if (priority) {
					srcList.unshift(src);
				} else {
					srcList.push(src);
				}
				if (ready && !started) {
					loadNextImg();
				}
			},
			loadNow: loadNow,
			ready: function() {
				loaded = true;
				ready = true;
				loadNextImg();
			}
		};
	})();
	if ($.windowLoaded) {
		$.imgPreLoad.ready();
	} else {
		$(window).bind('load', $.imgPreLoad.ready);
	}
})(window.webshims && webshims.$ || jQuery);
