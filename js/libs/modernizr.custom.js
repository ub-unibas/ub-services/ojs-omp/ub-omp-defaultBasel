/* Modernizr 2.6.2 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-fontface-backgroundsize-boxshadow-multiplebgs-opacity-cssanimations-csscolumns-csstransforms-csstransforms3d-csstransitions-canvas-audio-video-input-inputtypes-localstorage-sessionstorage-geolocation-inlinesvg-touch-mq-cssclasses-addtest-prefixed-teststyles-testprop-testallprops-prefixes-domprefixes-css_boxsizing-css_cubicbezierrange-css_displaytable-css_mediaqueries-elem_track
 */
;



window.Modernizr = (function(window, document, undefined) {

	var version = '2.6.2',

		Modernizr = {},

		enableClasses = true,

		docElement = document.documentElement,

		mod = 'modernizr',
		modElem = document.createElement(mod),
		mStyle = modElem.style,

		inputElem = document.createElement('input'),

		smile = ':)',

		toString = {}.toString,

		prefixes = ' -webkit- -moz- -o- -ms- '.split(' '),



		omPrefixes = 'Webkit Moz O ms',

		cssomPrefixes = omPrefixes.split(' '),

		domPrefixes = omPrefixes.toLowerCase().split(' '),

		ns = {
			'svg': 'http://www.w3.org/2000/svg'
		},

		tests = {},
		inputs = {},
		attrs = {},

		classes = [],

		slice = classes.slice,

		featureName,


		injectElementWithStyles = function(rule, callback, nodes, testnames) {

			var style, ret, node, docOverflow,
				div = document.createElement('div'),
				body = document.body,
				fakeBody = body || document.createElement('body');

			if (parseInt(nodes, 10)) {
				while (nodes--) {
					node = document.createElement('div');
					node.id = testnames ? testnames[nodes] : mod + (nodes + 1);
					div.appendChild(node);
				}
			}

			style = ['&#173;', '<style id="s', mod, '">', rule, '</style>'].join('');
			div.id = mod;
			(body ? div : fakeBody).innerHTML += style;
			fakeBody.appendChild(div);
			if (!body) {
				fakeBody.style.background = '';
				fakeBody.style.overflow = 'hidden';
				docOverflow = docElement.style.overflow;
				docElement.style.overflow = 'hidden';
				docElement.appendChild(fakeBody);
			}

			ret = callback(div, rule);
			if (!body) {
				fakeBody.parentNode.removeChild(fakeBody);
				docElement.style.overflow = docOverflow;
			} else {
				div.parentNode.removeChild(div);
			}

			return !!ret;

		},

		testMediaQuery = function(mq) {

			var matchMedia = window.matchMedia || window.msMatchMedia;
			if (matchMedia) {
				return matchMedia(mq).matches;
			}

			var bool;

			injectElementWithStyles('@media ' + mq + ' { #' + mod + ' { position: absolute; } }', function(node) {
				bool = (window.getComputedStyle ?
					getComputedStyle(node, null) :
					node.currentStyle)['position'] == 'absolute';
			});

			return bool;

		},
		_hasOwnProperty = ({}).hasOwnProperty,
		hasOwnProp;

	if (!is(_hasOwnProperty, 'undefined') && !is(_hasOwnProperty.call, 'undefined')) {
		hasOwnProp = function(object, property) {
			return _hasOwnProperty.call(object, property);
		};
	} else {
		hasOwnProp = function(object, property) {
			return ((property in object) && is(object.constructor.prototype[property], 'undefined'));
		};
	}


	if (!Function.prototype.bind) {
		Function.prototype.bind = function bind(that) {

			var target = this;

			if (typeof target != "function") {
				throw new TypeError();
			}

			var args = slice.call(arguments, 1),
				bound = function() {

					if (this instanceof bound) {

						var F = function() {};
						F.prototype = target.prototype;
						var self = new F();

						var result = target.apply(
							self,
							args.concat(slice.call(arguments))
						);
						if (Object(result) === result) {
							return result;
						}
						return self;

					} else {

						return target.apply(
							that,
							args.concat(slice.call(arguments))
						);

					}

				};

			return bound;
		};
	}

	function setCss(str) {
		mStyle.cssText = str;
	}

	function setCssAll(str1, str2) {
		return setCss(prefixes.join(str1 + ';') + (str2 || ''));
	}

	function is(obj, type) {
		return typeof obj === type;
	}

	function contains(str, substr) {
		return !!~('' + str).indexOf(substr);
	}

	function testProps(props, prefixed) {
		for (var i in props) {
			var prop = props[i];
			if (!contains(prop, "-") && mStyle[prop] !== undefined) {
				return prefixed == 'pfx' ? prop : true;
			}
		}
		return false;
	}

	function testDOMProps(props, obj, elem) {
		for (var i in props) {
			var item = obj[props[i]];
			if (item !== undefined) {

				if (elem === false) return props[i];

				if (is(item, 'function')) {
					return item.bind(elem || obj);
				}

				return item;
			}
		}
		return false;
	}

	function testPropsAll(prop, prefixed, elem) {

		var ucProp = prop.charAt(0).toUpperCase() + prop.slice(1),
			props = (prop + ' ' + cssomPrefixes.join(ucProp + ' ') + ucProp).split(' ');

		if (is(prefixed, "string") || is(prefixed, "undefined")) {
			return testProps(props, prefixed);

		} else {
			props = (prop + ' ' + (domPrefixes).join(ucProp + ' ') + ucProp).split(' ');
			return testDOMProps(props, prefixed, elem);
		}
	}



	tests['canvas'] = function() {
		var elem = document.createElement('canvas');
		return !!(elem.getContext && elem.getContext('2d'));
	};

	tests['touch'] = function() {
		var bool;

		if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
			bool = true;
		} else {
			injectElementWithStyles(['@media (', prefixes.join('touch-enabled),('), mod, ')', '{#modernizr{top:9px;position:absolute}}'].join(''), function(node) {
				bool = node.offsetTop === 9;
			});
		}

		return bool;
	};



	tests['geolocation'] = function() {
		return 'geolocation' in navigator;
	};



	tests['multiplebgs'] = function() {
		setCss('background:url(https://),url(https://),red url(https://)');

		return (/(url\s*\(.*?){3}/).test(mStyle.background);
	};
	tests['backgroundsize'] = function() {
		return testPropsAll('backgroundSize');
	};
	tests['boxshadow'] = function() {
		return testPropsAll('boxShadow');
	};



	tests['opacity'] = function() {
		setCssAll('opacity:.55');

		return (/^0.55$/).test(mStyle.opacity);
	};


	tests['cssanimations'] = function() {
		return testPropsAll('animationName');
	};


	tests['csscolumns'] = function() {
		return testPropsAll('columnCount');
	};

	tests['csstransforms'] = function() {
		return !!testPropsAll('transform');
	};


	tests['csstransforms3d'] = function() {

		var ret = !!testPropsAll('perspective');

		if (ret && 'webkitPerspective' in docElement.style) {

			injectElementWithStyles('@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}', function(node, rule) {
				ret = node.offsetLeft === 9 && node.offsetHeight === 3;
			});
		}
		return ret;
	};


	tests['csstransitions'] = function() {
		return testPropsAll('transition');
	};



	tests['fontface'] = function() {
		var bool;

		injectElementWithStyles('@font-face {font-family:"font";src:url("https://")}', function(node, rule) {
			var style = document.getElementById('smodernizr'),
				sheet = style.sheet || style.styleSheet,
				cssText = sheet ? (sheet.cssRules && sheet.cssRules[0] ? sheet.cssRules[0].cssText : sheet.cssText || '') : '';

			bool = /src/i.test(cssText) && cssText.indexOf(rule.split(' ')[0]) === 0;
		});

		return bool;
	};

	tests['video'] = function() {
		var elem = document.createElement('video'),
			bool = false;

		try {
			if (bool = !!elem.canPlayType) {
				bool = new Boolean(bool);
				bool.ogg = elem.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, '');

				bool.h264 = elem.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, '');

				bool.webm = elem.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, '');
			}

		} catch (e) {}

		return bool;
	};

	tests['audio'] = function() {
		var elem = document.createElement('audio'),
			bool = false;

		try {
			if (bool = !!elem.canPlayType) {
				bool = new Boolean(bool);
				bool.ogg = elem.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, '');
				bool.mp3 = elem.canPlayType('audio/mpeg;').replace(/^no$/, '');

				bool.wav = elem.canPlayType('audio/wav; codecs="1"').replace(/^no$/, '');
				bool.m4a = (elem.canPlayType('audio/x-m4a;') ||
					elem.canPlayType('audio/aac;')).replace(/^no$/, '');
			}
		} catch (e) {}

		return bool;
	};


	tests['localstorage'] = function() {
		try {
			localStorage.setItem(mod, mod);
			localStorage.removeItem(mod);
			return true;
		} catch (e) {
			return false;
		}
	};

	tests['sessionstorage'] = function() {
		try {
			sessionStorage.setItem(mod, mod);
			sessionStorage.removeItem(mod);
			return true;
		} catch (e) {
			return false;
		}
	};


	tests['inlinesvg'] = function() {
		var div = document.createElement('div');
		div.innerHTML = '<svg/>';
		return (div.firstChild && div.firstChild.namespaceURI) == ns.svg;
	};

	function webforms() {
		Modernizr['input'] = (function(props) {
			for (var i = 0, len = props.length; i < len; i++) {
				attrs[props[i]] = !!(props[i] in inputElem);
			}
			if (attrs.list) {
				attrs.list = !!(document.createElement('datalist') && window.HTMLDataListElement);
			}
			return attrs;
		})('autocomplete autofocus list placeholder max min multiple pattern required step'.split(' '));
		Modernizr['inputtypes'] = (function(props) {

			for (var i = 0, bool, inputElemType, defaultView, len = props.length; i < len; i++) {

				inputElem.setAttribute('type', inputElemType = props[i]);
				bool = inputElem.type !== 'text';

				if (bool) {

					inputElem.value = smile;
					inputElem.style.cssText = 'position:absolute;visibility:hidden;';

					if (/^range$/.test(inputElemType) && inputElem.style.WebkitAppearance !== undefined) {

						docElement.appendChild(inputElem);
						defaultView = document.defaultView;

						bool = defaultView.getComputedStyle &&
							defaultView.getComputedStyle(inputElem, null).WebkitAppearance !== 'textfield' &&
							(inputElem.offsetHeight !== 0);

						docElement.removeChild(inputElem);

					} else if (/^(search|tel)$/.test(inputElemType)) {} else if (/^(url|email)$/.test(inputElemType)) {
						bool = inputElem.checkValidity && inputElem.checkValidity() === false;

					} else {
						bool = inputElem.value != smile;
					}
				}

				inputs[props[i]] = !!bool;
			}
			return inputs;
		})('search tel url email datetime date month week time datetime-local number range color'.split(' '));
	}
	for (var feature in tests) {
		if (hasOwnProp(tests, feature)) {
			featureName = feature.toLowerCase();
			Modernizr[featureName] = tests[feature]();

			classes.push((Modernizr[featureName] ? '' : 'no-') + featureName);
		}
	}

	Modernizr.input || webforms();


	Modernizr.addTest = function(feature, test) {
		if (typeof feature == 'object') {
			for (var key in feature) {
				if (hasOwnProp(feature, key)) {
					Modernizr.addTest(key, feature[key]);
				}
			}
		} else {

			feature = feature.toLowerCase();

			if (Modernizr[feature] !== undefined) {
				return Modernizr;
			}

			test = typeof test == 'function' ? test() : test;

			if (typeof enableClasses !== "undefined" && enableClasses) {
				docElement.className += ' ' + (test ? '' : 'no-') + feature;
			}
			Modernizr[feature] = test;

		}

		return Modernizr;
	};


	setCss('');
	modElem = inputElem = null;


	Modernizr._version = version;

	Modernizr._prefixes = prefixes;
	Modernizr._domPrefixes = domPrefixes;
	Modernizr._cssomPrefixes = cssomPrefixes;

	Modernizr.mq = testMediaQuery;


	Modernizr.testProp = function(prop) {
		return testProps([prop]);
	};

	Modernizr.testAllProps = testPropsAll;


	Modernizr.testStyles = injectElementWithStyles;
	Modernizr.prefixed = function(prop, obj, elem) {
		if (!obj) {
			return testPropsAll(prop, 'pfx');
		} else {
			return testPropsAll(prop, obj, elem);
		}
	};


	docElement.className = docElement.className.replace(/(^|\s)no-js(\s|$)/, '$1$2') +

		(enableClasses ? ' js ' + classes.join(' ') : '');

	return Modernizr;

})(this, this.document);

// developer.mozilla.org/en/CSS/box-sizing
// github.com/Modernizr/Modernizr/issues/248

Modernizr.addTest("boxsizing", function() {
	return Modernizr.testAllProps("boxSizing") && (document.documentMode === undefined || document.documentMode > 7);
});


// Track element + Timed Text Track API
// http://www.w3.org/TR/html5/video.html#the-track-element
// http://www.w3.org/TR/html5/media-elements.html#text-track-api
//
// While IE10 has implemented the track element, IE10 does not expose the underlying APIs to create timed text tracks by JS (really sad)
// By Addy Osmani
Modernizr.addTest({
	texttrackapi: (typeof(document.createElement('video').addTextTrack) === 'function'),
	// a more strict test for track including UI support: document.createElement('track').kind === 'subtitles'
	track: ('kind' in document.createElement('track'))
});
// display: table and table-cell test. (both are tested under one name "table-cell" )
// By @scottjehl

// all additional table display values are here: http://pastebin.com/Gk9PeVaQ though Scott has seen some IE false positives with that sort of weak detection.
// more testing neccessary perhaps.

Modernizr.addTest("display-table", function() {

	var doc = window.document,
		docElem = doc.documentElement,
		parent = doc.createElement("div"),
		child = doc.createElement("div"),
		childb = doc.createElement("div"),
		ret;

	parent.style.cssText = "display: table";
	child.style.cssText = childb.style.cssText = "display: table-cell; padding: 10px";

	parent.appendChild(child);
	parent.appendChild(childb);
	docElem.insertBefore(parent, docElem.firstChild);

	ret = child.offsetLeft < childb.offsetLeft;
	docElem.removeChild(parent);
	return ret;
});

// cubic-bezier values can't be > 1 for Webkit until bug #45761 (https://bugs.webkit.org/show_bug.cgi?id=45761) is fixed
// By @calvein

Modernizr.addTest('cubicbezierrange', function() {
	var el = document.createElement('div');
	el.style.cssText = Modernizr._prefixes.join('transition-timing-function' + ':cubic-bezier(1,0,0,1.1); ');
	return !!el.style.length;
});


Modernizr.addTest('mediaqueries', Modernizr.mq('only all'));;
