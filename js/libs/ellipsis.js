/**
 * @module      ellipsis
 * @description ellipsis. Shorten the text to fit the visible container and add '...'
 * @author      Jose Luis Medina Burgos
 */

(function(window, document) {
	// var extend = 

	window.Ellipsis = function(el, options) {
		this.defaults = {
			multiline: true
		};

		if (el['ui-ellipsis']) {

		}

		this.el = el;
		this.options = this.defaults; // IHK.Helpers.extendObject(this.defaults, options);

		this._initialize();
	}

	window.Ellipsis.prototype = {
		/**
		 * @description initialize the tooltip view
		 *              asign the view template, bind listeners
		 * @param       options {Object}
		 * @return      {void}
		 */
		_initialize: function() {
			this.text = this._getText();
			this.el.innerHTML = this.text;
			this.checkSize = this.options.multiline ? this._isHigherThanEl : this._isWidtherThanEl;
			this._calculateText(this._cloneEl(this.el));
		},
		_getText: function() {
			var dataText = this.el.getAttribute('data-text');
			var result = '';

			if (dataText) {
				result = dataText;
			} else {
				result = this.el.innerHTML;
				this.el.setAttribute('data-text', result);
			}

			return result;
		},
		/**
		 * @description clone the context el and set some properties to make it invisible
		 *				here will be calculated how long the text should be, before showing it
		 * @param       el {DOM Element Object}
		 * @return      clone {DOM Element Object}
		 */
		_cloneEl: function(el) {
			var clone = el.cloneNode(true);
			var style = clone.style;
			var multiline = this.options.multiline;

			clone.style.opacity = '0';
			clone.style.position = 'absolute';
			clone.style.overflow = 'visible';
			clone.style.width = multiline ? Math.ceil(el.clientWidth) + 'px' : 'auto';
			clone.style.height = multiline ? 'auto' : Math.ceil(el.clientHeight) + 'px';
			clone.style.maxHeight = multiline ? 'none' : '';

			el.parentElement.appendChild(clone);
			return clone;
		},
		/**
		 * @description calculate the size of the text to be shown 
		 * @param       clone {DOM Element Object}
		 * @return      {void}
		 */
		_calculateText: function(clone) {
			while (this.text.length > 0 && this.checkSize(clone)) {
				this.text = this.text.substr(0, this.text.length - 10);
				clone.innerHTML = this.text + '...';
			}

			this.el.innerHTML = clone.innerHTML;
			clone.parentElement.removeChild(clone);

			// IHK.Emiter.fire(this.el, 'ellipsis:done');
		},
		/**
		 * @description 
		 * @param       el {DOM Element Object}
		 * @return      {Boolean}
		 */
		_isHigherThanEl: function(el) {
			return Math.floor(el.clientHeight) > this.el.clientHeight;
		},
		/**
		 * @description 
		 * @param       el {DOM Element Object}	
		 * @return      {Boolean}
		 */
		_isWidtherThanEl: function(el) {
			return Math.floor(el.clientWidth) > this.el.clientWidth;
		}
	}
})(window, window.document);
