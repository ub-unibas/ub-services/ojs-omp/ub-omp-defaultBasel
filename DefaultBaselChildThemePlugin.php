<?php

/**
 * @file plugins/themes/defaultBasel/DefaultBaselChildThemePlugin.php
 *
 * Copyright (c) 2014-2020 Simon Fraser University
 * Copyright (c) 2003-2020 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class DefaultBaselChildThemePlugin
 * @ingroup plugins_themes_default_basel
 *
 * @brief Default theme
 */

//import('lib.pkp.classes.plugins.ThemePlugin');
namespace APP\plugins\themes\defaultBasel;

use APP\core\Application;
use APP\core\Services;
use APP\facades\Repo;
use APP\file\PublicFileManager;
use APP\submission\Collector;
use APP\submission\Submission;
use APP\template\TemplateManager;
use PKP\config\Config;
use PKP\plugins\ThemePlugin;
use PKP\session\SessionManager;
use PKP\plugins\Hook;
use PKP\db\DAORegistry;

class DefaultBaselChildThemePlugin extends ThemePlugin {
	/**
	 * Initialize the theme's styles, scripts and hooks. This is only run for
	 * the currently active theme.
	 *
	 * @return null
	 */
	public function init() {

		// Initialize template manager
		//HookRegistry::register ('TemplateManager::display', array($this, 'loadTemplateData'));
		Hook::add ('TemplateManager::display', [$this, 'loadTemplateData']);


		// Initialize the parent theme
		$this->setParent('defaultthemeplugin');


		// Add custom styles
		$this->modifyStyle('stylesheet', array('addLess' => array('styles/index.less')));


		// Add Unibas css
		$this->addStyle('basel-css-icons', 'css/main.icons.css');


		// Add Unibas javascript
		//$this->addScript('__basic-behaviour', 'js/__basic-behaviour.js');
		$this->addScript('__base-modules', 'js/__base-modules.js');
		$this->addScript('__base-modules2', 'js/__base-modules2.js');
		$this->addScript('__enhanced-modules', 'js/__enhanced-modules.js');


		// Remove the typography options of the parent theme.
		// `removeOption` was introduced in OJS 3.0.2
		if (method_exists($this, 'removeOption')) {
			$this->removeOption('typography');
		}


		// Add the option for an accent color
		$this->addOption('accentColour', 'FieldColor', [
			'label' => __('plugins.themes.defaultBasel.option.accentColour.label'),
			'description' => __('plugins.themes.default.option.colour.description'),
			'default' => '#F7BC4A',
		]);

        /**
		// Load the Montserrat and Open Sans fonts
		$this->addStyle(
			'font',
			'//fonts.googleapis.com/css?family=Montserrat:400,700|Noto+Serif:400,400i,700,700i',
			array('baseUrl' => '')
		);
        */
		// Dequeue any fonts loaded by parent theme
		// `removeStyle` was introduced in OJS 3.0.2
		if (method_exists($this, 'removeStyle')) {
			$this->removeStyle('fontNotoSans');
			$this->removeStyle('fontNotoSerif');
			$this->removeStyle('fontNotoSansNotoSerif');
			$this->removeStyle('fontLato');
			$this->removeStyle('fontLora');
			$this->removeStyle('fontLoraOpenSans');
			$this->removeStyle('fontNotoSerif');
			$this->removeStyle('fontNotoSerif');
			$this->removeStyle('fontNotoSerif');
			$this->removeStyle('fontNotoSerif');
			$this->removeStyle('fontNotoSerif');
		}

		// Start with a fresh array of additionalLessVariables so that we can
		// ignore those added by the parent theme. This gets rid of @font
		// variable overrides from the typography option
		$additionalLessVariables = array();

		/**
		// Update colour based on theme option from parent theme
		if ($this->getOption('baseColour') !== '#1E6292') {
			$additionalLessVariables[] = '@bg-base:' . $this->getOption('baseColour') . ';';
			if (!$this->isColourDark($this->getOption('baseColour'))) {
				$additionalLessVariables[] = '@text-bg-base:rgba(0,0,0,0.84);';
			}
		}

		// Update accent colour based on theme option
		if ($this->getOption('accentColour') !== '#F7BC4A') {
			$additionalLessVariables[] = '@accent:' . $this->getOption('accentColour') . ';';
		}

		if ($this->getOption('baseColour') && $this->getOption('accentColour')) {
			$this->modifyStyle('stylesheet', array('addLessVariables' => join('', $additionalLessVariables)));
		}
        */


        $request = Application::get()->getRequest();
        $context = $request->getContext();
		if (isset($context)) {
			$contextId = $context->getId();
            $args = [];
            $availableContexts = Services::get('context')->getMany($args);

			$options = [[
				'value' => 0,
				'label' => 'none',
			]];
        	        foreach ($availableContexts as $availableContext) {

				if ( $contextId != $availableContext->getId() )
				{
					$subpressName = $availableContext->getLocalizedName()." (".$availableContext->getPath().")";
					$option = [
						'value' => $availableContext->getId(),
						'label' => $subpressName,
					];
					array_push($options, $option);
				}
			}

			$this->addOption('showSubPressItems', 'FieldOptions', [
				'label' => __('plugins.themes.defaultBasel.option.showSubPressItem.label'),
				'type' => 'radio',
				'options' => $options,
				'default' => 0,
			]);
		}

	}

	/**
	 * Get the display name of this plugin
	 * @return string
	 */
	function getDisplayName() {
		return __('plugins.themes.defaultBasel.name');
	}

	/**
	 * Get the description of this plugin
	 * @return string
	 */
	function getDescription() {
		return __('plugins.themes.defaultBasel.description');
	}

	// Add $journals variable to theme
	/**
	 * Fired when the `TemplateManager::display` hook is called.
	 *
	 * @param string $hookname
	 * @param array $args [$templateMgr, $template, $sendContentType, $charset, $output]
	 */
	public function loadTemplateData($hookName, $args) {

		// Retrieve the TemplateManager
		$templateMgr = $args[0];
		$template = $args[1];

		if ($template != 'frontend/pages/catalog.tpl') {
			$pressDao = DAORegistry::getDAO('PressDAO'); /* @var $pressDao PressDAO */
			$presses = $pressDao->getAll(true);
			$templateMgr->assign('presses', $presses);
            return false;
        }

		$request = Application::get()->getRequest();
        $context = $request->getContext();
		if (isset($context) != true ) {
			return false;
		}
        $contextId = $context->getId();
		$subpressContextId = $this->getOption('showSubPressItems');

//error_log("loadTemplateData called for catalog.tpl from context[".$contextId."] with subpress id[".$subpressContextId."]", 0);
		if ( $subpressContextId < 1 ) {
            return false;
		}

		$subpressContext = Services::get('context')->get($subpressContextId);

//error_log("\n\n###loadTemplateData got context[".$subpressContextId."]  getid[".$subpressContext->getId()."] path[".$subpressContext->getPath()."] name[".$subpressContext->getLocalizedName()."]", 0);

		$spTitle = $subpressContext->getLocalizedName();
		$spPath = $subpressContext->getPath();

        $collector = Repo::submission()
            ->getCollector()
            ->filterByContextIds([$subpressContextId])
            ->filterByStatus([Submission::STATUS_PUBLISHED])
            ->orderBy(Collector::ORDERBY_DATE_PUBLISHED, 'ASC')
            ->orderByFeatured();

        $count = 5;
        $offset = 0;

        $total = $collector->getCount();
        $submissions = $collector->limit($count)->offset($offset)->getMany();

		$featureDao = DAORegistry::getDAO('FeatureDAO'); /* @var $featureDao FeatureDAO */
        $featuredMonographIds = $featureDao->getSequencesByAssoc(Application::ASSOC_TYPE_PRESS, $subpressContextId);

		$featuredIds = [];
		foreach ($submissions as $submission) {
			if ( $featureDao->isFeatured( $submission->getId(), ASSOC_TYPE_PRESS, $subpressContextId )) {
				$featuredIds[$submission->getId()] = $submission->getId(); 
				error_log("loadTemplateData #### [".$submission->getId()."] featured ",0);
			}
		}

        if (count($featuredIds) > 0 ) {
	
		    $templateMgr->assign(array(
			    'subpressSubmissions' => $submissions->toArray(),
			    'subpressFeaturedIds' => $featuredIds,
                'subpressTitle'       => $spTitle,
                'subpressPath'        => $spPath,
            ));
        }

		return false;


	}


}

?>
