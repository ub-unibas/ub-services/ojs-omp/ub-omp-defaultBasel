{**
 * templates/frontend/components/pagedDownloadlink.tpl
 *
 * Copyright (c) 2014-2021 Simon Fraser University
 * Copyright (c) 2003-2021 John Willinsky
 * Distributed under the GNU GPL v3. For full terms see the file docs/COPYING.
 *
 * @brief Display a download link for files
 *
 * @uses $downloadFile SubmissionFile The download file object
 * @uses $monograph Monograph The monograph this file is attached to
 * @uses $publicationFormat PublicationFormat The publication format this file is attached to
 * @uses $currency Currency The currency object
 * @uses $useFilename string Whether or not to use the file name in link. Default is false and pub format name is used
 * @uses $page int the page number to use as an anchor
 *}

{assign var=publicationFormatId value=$publicationFormat->getBestId()}

{* Generate the download URL  op download or view *}
{if $publication->getId() === $monograph->getCurrentPublication()->getId()}
	{capture assign=downloadUrl}{url op="download" 
                                         path=$monograph->getBestId()|to_array:$publicationFormatId:$downloadFile->getBestId() 
                                         params=array('inline' => true) 
                                    }{/capture}
{else}
	{capture assign=downloadUrl}{url op="download" 
					 path=$monograph->getBestId()|to_array:"version":$publication->getId():$publicationFormatId:$downloadFile->getBestId()
                                         params=array('inline' => true) 
				    }{/capture}
{/if}

{* Display the download link as inline with a page anchor*}
{assign var=urlOpts value="#page=$page"}
<a href="{$downloadUrl}{$urlOpts}" class="cmp_download_link">
	{if $useFilename}
		{$downloadFile->getLocalizedData('name')}
	{else}
		{$publicationFormat->getLocalizedName()}
	{/if}
</a>
