{**
 * templates/frontend/pages/indexSite.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Site index.
 *
 *}

{assign var=journalsArray value=$journals->toArray()}
{include file="frontend/components/header_indexSite.tpl"}

<img src="public/titelbild.jpg" alt="Titelbild" width="6000" height="1317">
<img src="public/img/bild_ub_kunst_bib_omp.jpg" alt="bild_ub_kunst_bib" width="6000" height="1317">

<div class="page_index_site">

	{if $about}
		<div class="about_site">
			{$about|nl2br}
		</div>
	{/if}

	<div class="journals">
		<h2>
			{translate key="journal.journals"}
		</h2>
		{if !count($journals)}
			{translate key="site.noJournals"}
		{else}
			<ul>
				{foreach from=$journalsArray item=journal}
					{capture assign="url"}{url journal=$journal->getPath()}{/capture}
					{assign var="thumb" value=$journal->getLocalizedSetting('journalThumbnail')}
					{assign var="description" value=$journal->getLocalizedDescription()}
					<li{if $thumb} class="has_thumb"{/if}>
						{if $thumb}
							<div class="thumb">
								{*<a href="{$url|escape}">*}
									<img src="{$journalFilesPath}{$journal->getId()}/{$thumb.uploadName|escape:"url"}" {if $thumb.altText} alt="{$thumb.altText|escape}"{/if}>
								{*</a>*}
							</div>
						{/if}

						<div class="body">
							<h3>
								{$journal->getLocalizedName()}
								{*<a href="{$url|escape}" rel="bookmark">
									{$journal->getLocalizedName()}
								</a>*}
							</h3>
							{if $description}
								<div class="description">
									{$description|nl2br}
								</div>
							{/if}
							<a href="{$url|escape}">{translate key="plugins.themes.defaultBasel.goto.publication"}</a> &nbsp;
							<a href="{url|escape journal=$journal->getPath() page="issue" op="current"}">{translate key="site.journalCurrent"}</a>
							{*<ul class="links">
								<li class="view">
									<a href="{$url|escape}">
										{translate key="site.journalView"}
									</a>
								</li>
								<li class="current">
									<a href="{url|escape journal=$journal->getPath() page="issue" op="current"}">
										{translate key="site.journalCurrent"}
									</a>
								</li>
							</ul>*}
						</div>
					</li>
				{/foreach}
			</ul>
		{/if}
	</div>

</div><!-- .page -->

{include file="frontend/components/footer.tpl"}
